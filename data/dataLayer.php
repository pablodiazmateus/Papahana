<?php

//---------------------------------------------------------------------------------------------------------------------
// Connects to the DB.
function connect()
{
  $servernameDB = "localhost";
  $usernameDB = "root";
  $passwordDB = "root";
  $nameDB = "ThePapahana";

  // Create the connection.
  $connection = new mysqli($servernameDB, $usernameDB, $passwordDB, $nameDB);

  // If can not connects to the database return null, else return the connection.
  if($connection -> connect_error)
  {
    return null;
  }
  else
  {
    return $connection;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Checks if the email and the password are corrects.
function loginAction($email, $password)
{
  // Make a connection to the DB.
  $conn = connect();

  // If the connection were succesful make the query. Else return error statusTxt.
  if($conn != null)
  {

    $sql = "SELECT * FROM User WHERE email='$email' AND passwrd='$password'";
    $result = $conn->query($sql);

    //If the query returns more than 0 rows, return the data, else return error statusTxt.
    if($result->num_rows > 0)
    {
      while($row = $result->fetch_assoc())
      {
        $response = array("statusTxt" => "SUCCESS", "data" => $row);
      }

      return $response;
    }
    else
    {
      $response = array("statusTxt" => "User not found or incorrect Password");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Register a new user.
function registrationAction($firstName, $lastName, $email, $passwrd)
{
  $conn = connect();

  if($conn != null)
  {
    $sql = "SELECT * FROM User WHERE username='$username'";
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array("statusTxt" => "User already exists");
      return $response;
    }
    else
    {
      $sql =  "INSERT INTO User(fName, lName, email, passwrd)
              VALUES ('$firstName', '$lastName', '$email', '$passwrd')";

      if ($conn->query($sql) === TRUE)
      {
        $data = array("fName"=>$firstName, "lName"=>$lastName);
        $response = array("statusTxt" => "SUCCESS", "data" => $data);
        return $response;
      }
      else
      {
        $response = array("statusTxt" => "Error inserting in Users table");
        return $response;
      }
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Saves the schedule.
function scheduleAction($username, $lu, $ma, $mi, $ju, $vi, $hora)
{
  $conn = connect();

  if($conn != null)
  {
    $sql = "INSERT INTO Schedule(lunes, martes, miercoles, jueves, viernes, hora, idUser)
           VALUES ('$lu', '$ma', '$mi', '$ju', '$vi', '$hora', '$username')";
    if ($conn->query($sql) === TRUE)
    {
      $response = array("statusTxt" => "SUCCESS");
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "Error inserting in Users table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Saves a new project.
function createProjectAction($projectName, $description, $fechaFinal, $username)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $username;
    $sql = "INSERT INTO Project(id, createdBy, name, description, fechaFinal)
           VALUES ('$id', '$username', '$projectName', '$description', '$fechaFinal')";
    if ($conn->query($sql) === TRUE)
    {
      $sql = "INSERT INTO User_Project(idUser, idProject)
           VALUES ('$username', '$id')";
      if ($conn->query($sql) === TRUE)
      {
        $response = array("statusTxt" => "SUCCESS", "username" => $username);
        return $response;
      }
      else
      {
        $response = array("statusTxt" => "Error inserting in User_Projects table");
        return $response;
      }
    }
    else
    {
      $response = array("statusTxt" => "Error inserting in Project table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Deletes a new project.
function deleteProjectAction($projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "DELETE FROM User_Project WHERE idProject = '$id'";
    if ($conn->query($sql) === TRUE)
    {
      $sql = "DELETE FROM Project WHERE id = '$id'";
      if ($conn->query($sql) === TRUE)
      {
        $response = array("statusTxt" => "SUCCESS");
        return $response;
      }
      else
      {
        $response = array("statusTxt" => "Can't delete the project user");
        return $response;
      }
    }
    else
    {
      $response = array("statusTxt" => "Can't delete the project");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Return project's info.
function getProjectAction($projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "SELECT * FROM Project WHERE id = '$id'";
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      while($row = $result->fetch_assoc())
      {
        $response = array("statusTxt" => "SUCCESS", "name" => $row["name"], "description" => $row["description"],
                          "fechaFinal" => $row["fechaFinal"]);
      }
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "No Project Found");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Return project's collaborators.
function getCollaborators($projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "SELECT * FROM User_Project WHERE idProject = '$id'";
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array();
      //  Output data of each row
      while($row = $result->fetch_assoc())
      {
        $row = array('collaborator' => $row['idUser']);
        array_push($response, $row);
      }
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "No Users Found");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Gets all the user's projects.
function getProjectsAction($username)
{
  $conn = connect();

  if($conn != null)
  {
    $sql = "SELECT * FROM Project, User_Project WHERE User_Project.idUser = '$username' AND Project.id = User_Project.idProject";
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array();
      //  Output data of each row
      while($row = $result->fetch_assoc())
      {
        $row = array('name' => $row['name'], 'description' => $row['description'], 'fechaFinal' => $row['fechaFinal'], 
                     'createdBy' => $row['createdBy']);
        array_push($response, $row);
      }
      return $response;
    }
    else
    {
      $response = array();
      return $response;
    }

  }
  else
  {
    $response = array();
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Adds a user to a project.
function addUserAction($username, $projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "INSERT INTO User_Project(idUser, idProject)
           VALUES ('$username', '$id')";
    if ($conn->query($sql) === TRUE)
    {
      $response = array("statusTxt" => "SUCCESS");
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "Error inserting in User_Projects table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Gets all the project's Tasks.
function getTasksAction($projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "SELECT * FROM Tasks WHERE Tasks.idProject = '$id' ORDER BY Tasks.due_date";
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array();
      //  Output data of each row
      while($row = $result->fetch_assoc())
      {
        $row = array('collaborators' => $row['collaborators'], 'description' => $row['description'], 'due_date' => $row['due_date'],
                     'completed' => $row['completed']);
        array_push($response, $row);
      }
      return $response;
    }
    else
    {
      $response = array();
      return $response;
    }

  }
  else
  {
    $response = array();
    return $response;
  } 
}

//---------------------------------------------------------------------------------------------------------------------
// Adds a Task to an especific project.
function addTaskAction($collaborators, $description, $dueDate, $projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "INSERT INTO Tasks(collaborators, description, due_date, idProject)
           VALUES ('$collaborators', '$description', '$dueDate', '$id')";
    if ($conn->query($sql) === TRUE)
    {
      $response = array("statusTxt" => "SUCCESS");
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "Error inserting in Tasks table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");;
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Completes a to an especific project.
function completeTaskAction($collaborators, $description, $projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "UPDATE Tasks
            SET completed = TRUE 
            WHERE (collaborators = '$collaborators' AND description = '$description' AND idProject = '$id');";;
    if ($conn->query($sql) === TRUE)
    {
      $response = array("statusTxt" => "SUCCESS");
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "Error updating in Tasks table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");;
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Search collaborators in the database.
function searchCollaboratorAction($username, $search, $projectName, $createdBy)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "SELECT User.fName, User.lName, User.email FROM User_Project, User WHERE User.email NOT IN
           (SELECT User_Project.idUser FROM User_Project WHERE User_Project.idProject='$id') AND 
           User.email LIKE '$search%' AND User.email != '$username' GROUP BY User.email";
    
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array();
      while($row = $result->fetch_assoc())
      {
        $row = array('firstName' => $row['fName'], 'lastName' => $row['lName'], 'email' => $row['email']);
        array_push($response, $row);
      }
      return $response;
    }
    else
    {
      $response = array();
      return $response;
    }
  }
  else
  {
    $response = array();
    return $response;
  }
}

//---------------------------------------------------------------------------------------------------------------------
// edit Project.
function editProjectAction($projectName, $description, $createdBy, $fechaFinal)
{
  $conn = connect();

  if($conn != null)
  {
    $id = $projectName. $createdBy;
    $sql = "UPDATE Project
            SET description='$description', fechaFinal='$fechaFinal'
            WHERE Project.id='$id'";
    if ($conn->query($sql) === TRUE)
    {
      $response = array("statusTxt" => "SUCCESS", "projectName" => $projectName, "createdBy" => $createdBy);
      return $response;
    }
    else
    {
      $response = array("statusTxt" => "Error inserting in Project table");
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
}

function userScheduleAction($collaborator)
{
  $conn = connect();

  if($conn != null)
  {
    $sql = "SELECT * FROM Schedule WHERE idUser = '$collaborator' ORDER BY hora";
    
    $result = $conn->query($sql);

    if($result->num_rows > 0)
    {
      $response = array();
      while($row = $result->fetch_assoc())
      {
        $row = array("data" => $row);
        array_push($response, $row);
      }
      return $response;
    }
  }
  else
  {
    $response = array("statusTxt" => "Something went wrong with the connection to the database");
    return $response;
  }
  
}
?>