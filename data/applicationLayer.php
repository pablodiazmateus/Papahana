<?php
header('Accept: application/json');
require_once __DIR__ . '/dataLayer.php';

$action = $_POST["action"];
switch($action)
{
  case "COOKIE": checkCookies(); // ya
    break;
  case "SESSION": checkSession(); // ya
    break;
  case "LOGOUT": endUserSession();  
    break;
  case "LOGIN": verifyLogin(); // ya
    break;
  case "REGISTRATION": verifyRegistration(); // ya
    break;
  case "SCHEDULE": setSchedule(); // ya
    break;
  case "NEWPROJECT": createProject(); // ya
    break;
  case "DELETEPROJECT": deleteProject(); // ya
    break;
  case "ADDUSER": addUser(); 
    break;
  case "GETPROJECTS": getProjects(); // ya
    break;
  case "GETPROJECTINFO": getProjectInfo();
    break;
  case "GETTASKS": getTasks();
    break;
  case "ADDTASK": addTask();
    break;
  case "COMPLETETASK": completeTask();
    break;
  case "SEARCHCOLLABORATORS": searchCollaborator();
    break;
  case "EDITPROJECT": editProject();
    break;
  case "GETSCHEDULE": getSchedule();
    break;
}

function checkCookies()
{
  if (isset($_COOKIE["emailCookie"]) && isset($_COOKIE["passwordCookie"]))
  {
    echo json_encode(array("email" => $_COOKIE["emailCookie"], "password" => $_COOKIE["passwordCookie"]));
  }
  else
  {
    header('HTTP/1.1 406 Cookie has not been set yet');
    die(json_encode(array('statusTxt' => 'Cookie not set')));
  }
}

function checkSession()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    echo json_encode(array("firstName" => $_SESSION['fName'], "lastName" => $_SESSION['lName'], "email" => $_SESSION['email']));
  }
  else
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function endUserSession()
{
  session_destroy();

  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    header('HTTP/1.1 406 session active');
    die(json_encode(array('statusTxt' => 'Session still active')));
  }
  else
  {
    echo json_encode(array());
  }
}

function verifyLogin()
{
  $email = $_POST["email"];
  $password = $_POST["password"];
  $checked = $_POST["remember"];

  // Check the info in the database
  $result = loginAction($email, $password);

  if($result["statusTxt"] == "SUCCESS")
  {
    // Starting the session
    session_start();
    $_SESSION["email"] = $email;
    $_SESSION["fName"] = $result["data"]["fName"];
    $_SESSION["lName"] = $result["data"]["lName"];

    // Setting the cookies
    if($checked == "true")
    {
      setcookie("emailCookie", $email, time() + (3600*24*30));
      setcookie("passwordCookie", $password, time() + (3600*24*30));
    }

    // Retun data to the front-end
    $finalResponse = array("firstName" => $result["data"]["fName"], "lastName" => $result["data"]["lName"]);
    echo json_encode($finalResponse);
  }
  else
  {
    header('HTTP/1.1 406, User not found');
    die(json_encode($result));
  }
}

function verifyRegistration()
{
  $firstName = $_POST["firstName"];
  $lastName = $_POST["lastName"];
  $email = $_POST["email"];
  $password = $_POST["password"];

  // Try to insert the user in the DB.
  $result = registrationAction($firstName, $lastName, $email, $password);

  if ($result["statusTxt"] == "SUCCESS")
  {
    //Starting the session
    session_start();
    $_SESSION["email"] = $email;
    $_SESSION["fName"] = $firstName;
    $_SESSION["lName"] = $lastName;

    $finalResponse = array("firstName" => $result["data"]["fName"], "lastName" => $result["data"]["lName"]);
    echo (json_encode($finalResponse));
  }
  else
  {
    header('HTTP/1.1 406, User not found');
    die(json_encode($result));
  }
}

function setSchedule()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $username = $_SESSION["email"];
    $Lu1 = $_POST["Lu1"]; $Ma1 = $_POST["Ma1"]; $Mi1 = $_POST["Mi1"]; $Ju1 = $_POST["Ju1"]; $Vi1 = $_POST["Vi1"];
    $Lu2 = $_POST["Lu2"]; $Ma2 = $_POST["Ma2"]; $Mi2 = $_POST["Mi2"]; $Ju2 = $_POST["Ju2"]; $Vi2 = $_POST["Vi2"];
    $Lu3 = $_POST["Lu3"]; $Ma3 = $_POST["Ma3"]; $Mi3 = $_POST["Mi3"]; $Ju3 = $_POST["Ju3"]; $Vi3 = $_POST["Vi3"];
    $Lu4 = $_POST["Lu4"]; $Ma4 = $_POST["Ma4"]; $Mi4 = $_POST["Mi4"]; $Ju4 = $_POST["Ju4"]; $Vi4 = $_POST["Vi4"];
    $Lu5 = $_POST["Lu5"]; $Ma5 = $_POST["Ma5"]; $Mi5 = $_POST["Mi5"]; $Ju5 = $_POST["Ju5"]; $Vi5 = $_POST["Vi5"];
    $Lu6 = $_POST["Lu6"]; $Ma6 = $_POST["Ma6"]; $Mi6 = $_POST["Mi6"]; $Ju6 = $_POST["Ju6"]; $Vi6 = $_POST["Vi6"];
    $Lu7 = $_POST["Lu7"]; $Ma7 = $_POST["Ma7"]; $Mi7 = $_POST["Mi7"]; $Ju7 = $_POST["Ju7"]; $Vi7 = $_POST["Vi7"];
    $Lu8 = $_POST["LU8"]; $Ma8 = $_POST["Ma8"]; $Mi8 = $_POST["Mi8"]; $Ju8 = $_POST["Ju8"]; $Vi8 = $_POST["Vi8"];
    $Lu9 = $_POST["Lu9"]; $Ma9 = $_POST["Ma9"]; $Mi9 = $_POST["Mi9"]; $Ju9 = $_POST["Ju9"]; $Vi9 = $_POST["Vi9"];

    $result1 = scheduleAction($username, $Lu1, $Ma1, $Mi1, $Ju1, $Vi1, 730);
    $result2 = scheduleAction($username, $Lu2, $Ma2, $Mi2, $Ju2, $Vi2, 900);
    $result3 = scheduleAction($username, $Lu3, $Ma3, $Mi3, $Ju3, $Vi3, 1030);
    $result4 = scheduleAction($username, $Lu4, $Ma4, $Mi4, $Ju4, $Vi4, 1200);
    $result5 = scheduleAction($username, $Lu5, $Ma5, $Mi5, $Ju5, $Vi5, 1330);
    $result6 = scheduleAction($username, $Lu6, $Ma6, $Mi6, $Ju6, $Vi6, 1500);
    $result7 = scheduleAction($username, $Lu7, $Ma7, $Mi7, $Ju7, $Vi7, 1630);
    $result8 = scheduleAction($username, $Lu8, $Ma8, $Mi8, $Ju8, $Vi8, 1800);
    $result9 = scheduleAction($username, $Lu9, $Ma9, $Mi9, $Ju9, $Vi9, 1930);

    if($result1["statusTxt"] == "SUCCESS" && $result2["statusTxt"] == "SUCCESS" && $result3["statusTxt"] == "SUCCESS" &&
       $result4["statusTxt"] == "SUCCESS" && $result5["statusTxt"] == "SUCCESS" && $result6["statusTxt"] == "SUCCESS" &&
       $result7["statusTxt"] == "SUCCESS" && $result8["statusTxt"] == "SUCCESS" && $result9["statusTxt"] == "SUCCESS")
    {
      echo(json_encode(array("statusTxt" => "SUCCESS")));
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function createProject()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $description = $_POST["description"];
    $fechaFinal = $_POST["fechaFinal"];
    $username = $_SESSION["email"];

    $result = createProjectAction($projectName, $description, $fechaFinal, $username);

    if($result["statusTxt"] == "SUCCESS")
    {
      //echo $result["username"];
      echo json_encode($result);
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function deleteProject()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];

    $result = deleteProjectAction($projectName, $createdBy);

    if($result["statusTxt"] == "SUCCESS")
    {
      echo(json_encode(array("statusTxt" => "SUCCESS")));
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function addUser()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];
    $username = $_POST['email'];

    $result = addUserAction($username, $projectName, $createdBy);

    if($result["statusTxt"] == "SUCCESS")
    {
      echo(json_encode(array("statusTxt" => "SUCCESS")));
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function getProjects()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $username = $_SESSION['email'];

    $result = getProjectsAction($username);

    echo(json_encode($result));
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function getTasks()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];

    $result = getTasksAction($projectName, $createdBy);

    echo(json_encode($result));
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function addTask()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $collaborators = $_POST['collaborators'];
    $description = $_POST['description'];
    $dueDate = $_POST["dueDate"];
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];

    $result = addTaskAction($collaborators, $description, $dueDate, $projectName, $createdBy);

    echo(json_encode($result));
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  } 
}

function completeTask()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $collaborators = $_POST['collaborators'];
    $description = $_POST['description'];
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];

    $result = completeTaskAction($collaborators, $description, $projectName, $createdBy);

    if($result["statusTxt"] == "SUCCESS")
    {
      echo(json_encode(array("statusTxt" => "SUCCESS")));
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

function getProjectInfo()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];

    $result = getProjectAction($projectName, $createdBy);
    $collaborators = getCollaborators($projectName, $createdBy);

    if($result["statusTxt"] == "SUCCESS")
    {
      $finalResponse = array("statusTxt" => "SUCCESS", "name" => $result["name"], "description" => $result["description"],
                             "fechaFinal" => $result["fechaFinal"], "collaborators" => $collaborators);
      echo(json_encode($finalResponse));
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}


function searchCollaborator()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $username = $_SESSION['email'];
    $search = $_POST['search'];
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"];
    $result = searchCollaboratorAction($username, $search, $projectName, $createdBy);
    echo json_encode($result);
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}


function editProject()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $description = $_POST["description"];
    $createdBy = $_POST["createdBy"];
    $fechaFinal = $_POST["fechaFinal"];

    $result = editProjectAction($projectName, $description, $createdBy, $fechaFinal);

    if($result["statusTxt"] == "SUCCESS")
    {
      echo json_encode($result);
    }
    else
    {
      header('HTTP/1.1 406, Error in DB');
      die(json_encode($result));
    }
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die(json_encode(array('statusTxt' => 'Session has expired')));
  }
}

//---------------------------------------------------------------------------------------------------------------------
// Get the project's when to meet.
function getSchedule()
{
  session_start();
  if(isset($_SESSION['fName']) && isset($_SESSION['lName']) && isset($_SESSION['email']))
  {
    $projectName = $_POST["projectName"];
    $createdBy = $_POST["createdBy"]; 
    $collaborators = getCollaborators($projectName, $createdBy);
    $response = array();
    for($i = 0; $i < count($collaborators); ++$i) 
    {
      $collaborator = $collaborators[$i]['collaborator'];
      $userSchedule = userScheduleAction($collaborator);
      array_push($response, $userSchedule);
    }
    $finalResponse = array("data" => $response, "TOTAL" => count($collaborators));
    echo json_encode($finalResponse);
  }
  else 
  {
    header('HTTP/1.1 406 Session has expired, you will be redirected to the login');
    die (json_encode(array('statusTxt' => 'Session has expired')));
  }
}

?>