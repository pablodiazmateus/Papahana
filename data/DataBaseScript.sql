CREATE TABLE User (
fName VARCHAR(30) NOT NULL,
lName VARCHAR(30) NOT NULL,
email VARCHAR(50) PRIMARY KEY,
passwrd VARCHAR(50) NOT NULL
);

CREATE TABLE Project (
  id VARCHAR(80) PRIMARY KEY,
  createdBy VARCHAR(50) NOT NULL,
  name VARCHAR(30) NOT NULL,
  description VARCHAR(255) NOT NULL,
  fechaFinal DATE NOT NULL,
  FOREIGN KEY (createdBy) REFERENCES User(email)
);

CREATE TABLE User_Project (
  idUser VARCHAR(50) NOT NULL,
  idProject varchar(80) NOT NULL,
  FOREIGN KEY (idUser) REFERENCES User(email),
  FOREIGN KEY (idProject) REFERENCES Project(id), 
  CONSTRAINT pk_UserProject PRIMARY KEY (idUser, idProject)
);

CREATE TABLE Tasks (
  collaborators VARCHAR(255) NOT NULL,
  description VARCHAR(255) NOT NULL,
  due_date DATE NOT NULL,
  idProject varchar(80) NOT NULL,
  completed BOOL NOT NULL,
  FOREIGN KEY(idProject) REFERENCES Project(id),
  CONSTRAINT pk_UserProject PRIMARY KEY (collaborators, idProject, description)
);


CREATE TABLE Schedule (
  lunes INT NOT NULL,
  martes INT NOT NULL,
  miercoles INT NOT NULL,
  jueves INT NOT NULL,
  viernes INT NOT NULL,
  hora INT NOT NULL,
  idUser VARCHAR(50) NOT NULL,
  CONSTRAINT pk_Schedule PRIMARY KEY (hora, idUser),
  FOREIGN KEY (idUser) REFERENCES User(email)
);


