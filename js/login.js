$(document).ready(function() {
  var dataToSend = {"action": "COOKIE"};

  $.ajax({
    url: "data/applicationLayer.php",
    type: "POST",
    data: dataToSend,
    dataType: "json",
    success: function(cookieJson) {
      $("#email").val(cookieJson.email);
      $("#password").val(cookieJson.password);
    },
    error: function(errorMsg) 
    {

    }
  });

  $("#submitLogin").on("click", function(e) { 
    e.preventDefault();

    var val = true;             
    if ($("#email").val() == "") {
      // El vExistingUsername es un span que te dice que el username está mal o no existe .... necesitaríamos meterlo.
      // $("#vExistingUsername").text("Please enter a valid Username");
      val = false;
    }
    else {
      // $("#vExistingUsername").text("");
    }

    if ($("#password").val() == ""){
      // el vExistingPass es un span que te dice que el pass está mal o no existe .... necesitaríamos meterlo
      // $("#vExistingPass").text("Please enter a valid Pass");
      val = false;
    }
    else {
      // $("#vExistingPass").text(""); 
    }
    if(val){

      var dataToSend = {
        "action": "LOGIN",
        "email": $("#email").val(),
        "password": $("#password").val(),
        "remember": $("#check").is(":checked")
      };

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(jsonObject) {
          window.location.replace("home.php");
        },
        error: function(errorMsg) {
          alert("El usuario o la contraseña no son válidos");
        }
      });                 
    }
  });

  $("#submitRegistration").click(function(e) {
    e.preventDefault();
    var valReg = true;     

    // Todo lo que tenga v al principio es un span para verificar si pusieron cosas vacías y anuncie al usuario ... pero pondríamos los iconitos del responsive error no ? 

    if ($("#firstName").val() == ""){
      //$("#vFName").text("Please enter a valid first name");
      valReg = false;
    }
    else{
      //$("#vFName").text("");
    }


    if ($("#lastName").val() == ""){
      //$("#vLName").text("Please enter a valid last name");
      valReg = false;
    }
    else{
      //$("#vLName").text("");
    }

    if ($("#newEmail").val() == ""){
      //$("#vEmail").text("Please enter a valid email");
      valReg = false;
    }
    else{
      //$("#vEmail").text("");
    }


    if ($("#newPassword").val() == ""){
      // $("#vNewPass").text("Please enter a valid password");
      valReg = false;
    }
    else{
      // $("#vNewPass").text("");
    }


    if ($("#newPasswordConfirmation").val() != $("#newPassword").val()){
      //$("#vNewPassCon").text("Please enter the same password");
      valReg = false;
    }
    else{
      // $("#vNewPassCon").text("");
    }

    if(valReg) {

      var dataToSend = {
        "action" : "REGISTRATION",
        "firstName": $("#firstName").val(),
        "lastName": $("#lastName").val(),
        "email": $("#newEmail").val(),
        "password": $("#newPassword").val(),
      };

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(response) {
          window.location.replace("schedule.html");
        },
        error: function(errorMsg) {
          alert(errorMsg.statusTxt);
        } 
      });
    }
  }); 
});