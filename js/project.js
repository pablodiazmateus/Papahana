function loadTasks()
{
  var dataToSend = { "action": "GETTASKS", 
                    "projectName": $("#projectName").text().substring(6) ,
                    "createdBy": $("#createdBy").text().substring(12)};
  $.ajax({
    type: "POST",
    url: "data/applicationLayer.php",
    data: dataToSend,
    dataType: "json",
    contentType: "application/x-www-form-urlencoded",
    success: function(jsonData) {
      var collaborators;
      var description;
      var finalDate;
      var completed;
      var currentHTML;
      var progressBarHTML;
      var currentLength = jsonData.length;
      var total = jsonData.total;
      var totalCompleted = jsonData.totalCompleted;
      var progressValue = cuenta/currentLength*100;

      if (currentLength == 0){
        currentHTML = '<tr> <th> Task </th> <th> Due date </th> <th> Responsible </th></tr>';
        // progressBarHTML = '<progress class="progress" value="0" max="100"></progress>';
      }
      else {
        currentHTML = '<tr> <th> Task </th> <th> Due date </th> <th> Responsible </th> <th> Check </th> </tr>';
      }
      var cuenta = 0;

      for (i = 0; i < currentLength; i++) {
        collaborators = jsonData[i].collaborators;
        description = jsonData[i].description;
        finalDate = jsonData[i].due_date;
        completed = jsonData[i].completed;

        currentHTML += '<tr> <td class="tDescription">' + description + '</td> <td class="tFinalDate">' + finalDate + '</td> <td class="tResponsible">' +         collaborators + '</td>'; 

        if(completed == 1) {
          currentHTML += '<td> <span> <a class="button is-success completed" disabled> <span class="icon is-small"> <i class="fa fa-check"' + 
            'aria-hidden="true"></i> </span></a> </span> </td> </tr>';
          cuenta ++;
        }
        else {
          currentHTML += '<td> <span> <a class="button is-warning uncompleted"> <span class="icon is-small"> <i class="fa fa-check"' + 
            'aria-hidden="true"></i> </span></a> </span> </td> </tr>';
        }

      }
      currentHTML += '<tr><td><input type="text" id="taskDescription" /></td><td> <input type="text" id="taskDate"/>' +
        '</td><td> <input type="text" id="taskResponsible" /> </td></tr>';
      currentHTML += '<tr><td><input type="submit" class="button is-info" value="Add New Task" id="addTask"/></td></tr>';
      progressBarHTML = '<progress class="progress is-success" value="'+  cuenta/currentLength*100 + '" max="100"></progress>';

      //      alert(cuenta/currentLength*100);

      $("#taskTable").html(currentHTML);
      $("#progressBar").html(progressBarHTML)
    },
    error: function(errormsg) {
      alert(errormsg.statusText);
    }
  });
}

function changeColors(id, dato, total)
{
  if(dato/total >= 0.9) {
    $("#"+id).addClass("colorTodos");
  }
  else if(dato/total >= 0.75) {
    $("#"+id).addClass("colorCasiTodos");
  }
  else if(dato/total >= 0.50) {
    $("#"+id).addClass("colorMedia");
  }
  else if(dato/total >= 0.30) {
    $("#"+id).addClass("colorBaja");
  }
  else{
    $("#"+id).addClass("colorMuyBaja");
  }
}

$(document).ready(function () {

    $("#logout").on("click", function() {
      var dataToSend = {"action": "LOGOUT"};

      $.ajax({
        type: "POST",
        url: "data/applicationLayer.php",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(sessionObjson) {
          alert("Thank you for using Papahana :) ")
          window.location.replace("index.html");
        },
        error: function(errormsg)
        {
          alert(errormsg.statusTxt);
          if(errormsg.statusTxt == 'Session has expired')
          {
            window.location.replace("index.html");
          }
        }
      });
    });

    $("#tabs > li").click(function() {
      var identifier = $(this).attr("id");

      $("#tabs > li").removeClass('selected');
      $("#tabs > li").removeClass('is-active');
      $(this).addClass('selected');
      $(this).addClass('is-active');    

      $("#tabcontent > div").removeClass('active');
      $("#tabcontent > #content-" + identifier).addClass('active'); 
    });

    var dataToSend = {"action": "GETPROJECTINFO", 
                      "projectName": $("#projectName").text().substring(6),
                      "createdBy": $("#createdBy").text().substring(12)};
    $.ajax({
      url: "data/applicationLayer.php",
      type: "POST",
      data: dataToSend,
      dataType: "json",
      contentType: "application/x-www-form-urlencoded",
      success: function(response) 
      {
        $("#description").html("<strong>Description:</strong>  " + response.description);
        $("#dueDate").html("<strong>Due date:</strong>  " + response.fechaFinal);
        var collaborators = "";
        var collaborator;
        for (var i = 0; i<response.collaborators.length; i++){
          collaborator = response.collaborators[i].collaborator + ", ";
          collaborators += collaborator;
        }
        collaborators = collaborators.substring(0, collaborators.length-2);
        $("#collaborators").html("<strong>Collaborators:</strong>  " + collaborators);
      },
      error: function(errorMsg) 
      {
        alert(errorMsg.statusTxt);
      } 
    });

    loadTasks();

    $("#taskTable").on("click", "#addTask", function(){
      var val = true;             
      if ($("#taskDescription").val() == "") {
        val = false;
      }
      if ($("#taskDate").val() == ""){
        val = false;
      }
      if ($("#taskResponsible").val() == ""){
        val = false;
      }

      if(val){

        var dataToSend = {
          "action": "ADDTASK",
          "collaborators": $("#taskResponsible").val(),
          "description": $("#taskDescription").val(),
          "dueDate": $("#taskDate").val(),
          "projectName": $("#projectName").text().substring(6),
          "createdBy": $("#createdBy").text().substring(12)
        };

        $.ajax({
          url: "data/applicationLayer.php",
          type: "POST",
          data: dataToSend,
          dataType: "json",
          contentType: "application/x-www-form-urlencoded",
          success: function(jsonObject) {
            loadTasks();
          },
          error: function(errorMsg) {
            alert(errorMsg.statusTxt);
          }
        });                 
      }
    });

    $("#taskTable").on("click", ".uncompleted", function(){
      var dataToSend = {
        "action": "COMPLETETASK",
        "collaborators": $(this).parent().parent().parent().find("td.tResponsible").text(),
        "description": $(this).parent().parent().parent().find("td.tDescription").text(),
        "projectName": $("#projectName").text().substring(6),
        "createdBy": $("#createdBy").text().substring(12)
      };

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(jsonObject) {
          loadTasks();
        },
        error: function(errorMsg) {
          alert(errorMsg.statusTxt);
        }
      }); 
    });

    $("#searchButton").click(function () {
      var dataToSend = {"action": "SEARCHCOLLABORATORS", 
                        "search": $("#search").val(),
                        "projectName": $("#projectName").text().substring(6),
                        "createdBy": $("#createdBy").text().substring(12)
                       };
      $.ajax({
        type: 'POST',
        url: 'data/applicationLayer.php',
        dataType: 'json',
        data: dataToSend,
        contentType: "application/x-www-form-urlencoded",
        success: function(jsonData) {
          var currentHTML = '<tr><th>Name</th><th>Email</th></tr>';
          var name;
          var username;
          var email;
          var currentLength = jsonData.length;
          if(currentLength == 0) {
            currentHTML = "<tr>" + "<td>" + "No Results :(" + "</td>" + "</tr>";
          }
          for (i = 0; i < currentLength; i++) {
            name = jsonData[i].firstName + " " + jsonData[i].lastName;
            email = jsonData[i].email;
            currentHTML += "<tr>" + "<td>" + name + "</td>" + "<td class='searchEmail'>" + email + "</td>" + "<td>" + 
              "<span> <a class='button is-success search'> <span class='icon is-small'> <i class='fa fa-check'" + 
              "aria-hidden='true'></i> </span></a> </span" + "</td>" + "</tr>";
          }

          $("#searchTable").html(currentHTML);
        },
        error: function(errorMsg){
          alert(errorMsg.statusText);
          if (errorMsg.message=='Session has expired') {
            window.location.replace("index.html");
          }
        }
      });
    });

    $("#tab2").click( function () {
      var dataToSend = {"action": "GETPROJECTINFO", 
                        "projectName": $("#projectName").text().substring(6) ,
                        "createdBy": $("#createdBy").text().substring(12)};

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(response) 
        {
          $("#newProjectName").val(response.name);
          $("#finalDate").val(response.fechaFinal);
          $("#newDescription").val(response.description);
        },
        error: function(errorMsg) 
        {
          alert(errorMsg.statusTxt);
        } 
      });
    });

    $("#updateProject").click( function (e) {
      e.preventDefault();
      var dataToSend = {"action": "EDITPROJECT", 
                        "projectName": $("#projectName").text().substring(6),
                        "fechaFinal": $("#finalDate").val(),
                        "description": $("#newDescription").val(),
                        "createdBy": $("#createdBy").text().substring(12)};

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(response) 
        {
          var urlToGo = "project.php?" + "name=" + response.projectName + "&CreatedBy=" + response.createdBy;
          window.location.replace(urlToGo);
        },
        error: function(errorMsg) 
        {
          alert(errorMsg.statusTxt);
        } 
      });
    });

    $("#searchTable").on("click", ".search", function(e) {
      e.preventDefault();

      var dataToSend = {
        "action": "ADDUSER",
        "email": $(this).parent().parent().parent().find("td.searchEmail").text(),
        "projectName": $("#projectName").text().substring(6),
        "createdBy": $("#createdBy").text().substring(12)


      };

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(jsonObject) {
          var projectName = $("#projectName").text().substring(6);
          var createdBy = $("#createdBy").text().substring(12);
          var urlToGo = "project.php?" + "name=" + projectName + "&CreatedBy=" + createdBy;
          window.location.replace(urlToGo);
        },
        error: function(errorMsg) {
          alert(errorMsg.statusTxt);
        }
      }); 
    });

    $("#tab3").click( function () {
      var dataToSend = {"action": "GETSCHEDULE", 
                        "projectName": $("#projectName").text().substring(6) ,
                        "createdBy": $("#createdBy").text().substring(12)};

      $.ajax({
        url: "data/applicationLayer.php",
        type: "POST",
        data: dataToSend,
        dataType: "json",
        contentType: "application/x-www-form-urlencoded",
        success: function(response) 
        {
          var Lu1 = 0; var Ma1 = 0; var Mi1 = 0; var Ju1 = 0; var Vi1 = 0;
          var Lu2 = 0; var Ma2 = 0; var Mi2 = 0; var Ju2 = 0; var Vi2 = 0;
          var Lu3 = 0; var Ma3 = 0; var Mi3 = 0; var Ju3 = 0; var Vi3 = 0;
          var Lu4 = 0; var Ma4 = 0; var Mi4 = 0; var Ju4 = 0; var Vi4 = 0;
          var Lu5 = 0; var Ma5 = 0; var Mi5 = 0; var Ju5 = 0; var Vi5 = 0;
          var Lu6 = 0; var Ma6 = 0; var Mi6 = 0; var Ju6 = 0; var Vi6 = 0;
          var Lu7 = 0; var Ma7 = 0; var Mi7 = 0; var Ju7 = 0; var Vi7 = 0;
          var Lu8 = 0; var Ma8 = 0; var Mi8 = 0; var Ju8 = 0; var Vi8 = 0;
          var Lu9 = 0; var Ma9 = 0; var Mi9 = 0; var Ju9 = 0; var Vi9 = 0;

          for(var i = 0; i < response.data.length; i++)
          {
            Lu1 += parseInt(response.data[i][0].data.lunes);     Ma1 += parseInt(response.data[i][0].data.martes); 
            Mi1 += parseInt(response.data[i][0].data.miercoles); Ju1 += parseInt(response.data[i][0].data.jueves); 
            Vi1 += parseInt(response.data[i][0].data.viernes);

            Lu2 += parseInt(response.data[i][1].data.lunes);     Ma2 += parseInt(response.data[i][1].data.martes); 
            Mi2 += parseInt(response.data[i][1].data.miercoles); Ju2 += parseInt(response.data[i][1].data.jueves); 
            Vi2 += parseInt(response.data[i][1].data.viernes);

            Lu3 += parseInt(response.data[i][2].data.lunes);     Ma3 += parseInt(response.data[i][2].data.martes); 
            Mi3 += parseInt(response.data[i][2].data.miercoles); Ju3 += parseInt(response.data[i][2].data.jueves); 
            Vi3 += parseInt(response.data[i][2].data.viernes);

            Lu4 += parseInt(response.data[i][3].data.lunes);     Ma4 += parseInt(response.data[i][3].data.martes); 
            Mi4 += parseInt(response.data[i][3].data.miercoles); Ju4 += parseInt(response.data[i][3].data.jueves); 
            Vi4 += parseInt(response.data[i][3].data.viernes);

            Lu5 += parseInt(response.data[i][4].data.lunes);     Ma5 += parseInt(response.data[i][4].data.martes); 
            Mi5 += parseInt(response.data[i][4].data.miercoles); Ju5 += parseInt(response.data[i][4].data.jueves); 
            Vi5 += parseInt(response.data[i][4].data.viernes);

            Lu6 += parseInt(response.data[i][5].data.lunes);     Ma6 += parseInt(response.data[i][5].data.martes); 
            Mi6 += parseInt(response.data[i][5].data.miercoles); Ju6 += parseInt(response.data[i][5].data.jueves); 
            Vi6 += parseInt(response.data[i][5].data.viernes);

            Lu7 += parseInt(response.data[i][6].data.lunes);     Ma7 += parseInt(response.data[i][6].data.martes); 
            Mi7 += parseInt(response.data[i][6].data.miercoles); Ju7 += parseInt(response.data[i][6].data.jueves); 
            Vi7 += parseInt(response.data[i][6].data.viernes);

            Lu8 += parseInt(response.data[i][7].data.lunes);     Ma8 += parseInt(response.data[i][7].data.martes); 
            Mi8 += parseInt(response.data[i][7].data.miercoles); Ju8 += parseInt(response.data[i][7].data.jueves);
            Vi8 += parseInt(response.data[i][7].data.viernes);

            Lu9 += parseInt(response.data[i][8].data.lunes);     Ma9 += parseInt(response.data[i][8].data.martes); 
            Mi9 += parseInt(response.data[i][8].data.miercoles); Ju9 += parseInt(response.data[i][8].data.jueves);
            Vi9 += parseInt(response.data[i][8].data.viernes);
          }

          $("#Lu1").html("<strong>" + Lu1 + "</strong>");
          changeColors("Lu1", Lu1, response.TOTAL);
          
          $("#Lu2").html("<strong>" + Lu2 + "</strong>");
          changeColors("Lu2", Lu2, response.TOTAL);
          
          $("#Lu3").html("<strong>" + Lu3 + "</strong>");
          changeColors("Lu3", Lu3, response.TOTAL);
          
          $("#Lu4").html("<strong>" + Lu4 + "</strong>");
          changeColors("Lu4", Lu4, response.TOTAL);
          
          $("#Lu5").html("<strong>" + Lu5 + "</strong>");
          changeColors("Lu5", Lu5, response.TOTAL);
          
          $("#Lu6").html("<strong>" + Lu6 + "</strong>");
          changeColors("Lu6", Lu6, response.TOTAL);
          
          $("#Lu7").html("<strong>" + Lu7 + "</strong>");
          changeColors("Lu7", Lu7, response.TOTAL);
          
          $("#Lu8").html("<strong>" + Lu8 + "</strong>");
          changeColors("Lu8", Lu8, response.TOTAL);
          
          $("#Lu9").html("<strong>" + Lu9 + "</strong>");
          changeColors("Lu9", Lu9, response.TOTAL);
          
          $("#Ma1").html("<strong>" + Ma1 + "</strong>");
          changeColors("Ma1", Ma1, response.TOTAL);
          
          $("#Ma2").html("<strong>" + Ma2 + "</strong>");
          changeColors("Ma2", Ma2, response.TOTAL);
          
          $("#Ma3").html("<strong>" + Ma3 + "</strong>");
          changeColors("Ma3", Ma3, response.TOTAL);
          
          $("#Ma4").html("<strong>" + Ma4 + "</strong>");
          changeColors("Ma4", Ma4, response.TOTAL);
          
          $("#Ma5").html("<strong>" + Ma5 + "</strong>");
          changeColors("Ma5", Ma5, response.TOTAL);
          
          $("#Ma6").html("<strong>" + Ma6 + "</strong>");
          changeColors("Ma6", Ma6, response.TOTAL);
          
          $("#Ma7").html("<strong>" + Ma7 + "</strong>");
          changeColors("Ma7", Ma7, response.TOTAL);
          
          $("#Ma8").html("<strong>" + Ma8 + "</strong>");
          changeColors("Ma8", Ma8, response.TOTAL);
          
          $("#Ma9").html("<strong>" + Ma9 + "</strong>");
          changeColors("Ma9", Ma9, response.TOTAL);
          
          
          $("#Mi1").html("<strong>" + Mi1 + "</strong>");
          changeColors("Mi1", Mi1, response.TOTAL);
          
          $("#Mi2").html("<strong>" + Mi2 + "</strong>");
          changeColors("Mi2", Mi2, response.TOTAL);
          
          $("#Mi3").html("<strong>" + Mi3 + "</strong>");
          changeColors("Mi3", Mi3, response.TOTAL);
          
          $("#Mi4").html("<strong>" + Mi4 + "</strong>");
          changeColors("Mi4", Mi4, response.TOTAL);
          
          $("#Mi5").html("<strong>" + Mi5 + "</strong>");
          changeColors("Mi5", Mi5, response.TOTAL);
          
          $("#Mi6").html("<strong>" + Mi6 + "</strong>");
          changeColors("Mi6", Mi6, response.TOTAL);
          
          $("#Mi7").html("<strong>" + Mi7 + "</strong>");
          changeColors("Mi7", Mi7, response.TOTAL);
          
          $("#Mi8").html("<strong>" + Mi8 + "</strong>");
          changeColors("Mi8", Mi8, response.TOTAL);
          
          $("#Mi9").html("<strong>" + Mi9 + "</strong>");
          changeColors("Mi9", Mi9, response.TOTAL);
          
          $("#Ju1").html("<strong>" + Ju1 + "</strong>");
          changeColors("Ju1", Ju1, response.TOTAL);
          
          $("#Ju2").html("<strong>" + Ju2 + "</strong>");
          changeColors("Ju2", Ju2, response.TOTAL);
          
          $("#Ju3").html("<strong>" + Ju3 + "</strong>");
          changeColors("Ju3", Ju3, response.TOTAL);
          
          $("#Ju4").html("<strong>" + Ju4 + "</strong>");
          changeColors("Ju4", Ju4, response.TOTAL);
          
          $("#Ju5").html("<strong>" + Ju5 + "</strong>");
          changeColors("Ju5", Ju5, response.TOTAL);
          
          $("#Ju6").html("<strong>" + Ju6 + "</strong>");
          changeColors("Ju6", Ju6, response.TOTAL);
          
          $("#Ju7").html("<strong>" + Ju7 + "</strong>");
          changeColors("Ju7", Ju7, response.TOTAL);
          
          $("#Ju8").html("<strong>" + Ju8 + "</strong>");
          changeColors("Ju8", Ju8, response.TOTAL);
          
          $("#Ju9").html("<strong>" + Ju9 + "</strong>");
          changeColors("Ju9", Ju9, response.TOTAL);
          
          $("#Vi1").html("<strong>" + Vi1 + "</strong>");
          changeColors("Vi1", Vi1, response.TOTAL);
          
          $("#Vi2").html("<strong>" + Vi2 + "</strong>");
          changeColors("Vi2", Vi2, response.TOTAL);
          
          $("#Vi3").html("<strong>" + Vi3 + "</strong>");
          changeColors("Vi3", Vi3, response.TOTAL);
          
          $("#Vi4").html("<strong>" + Vi4 + "</strong>");
          changeColors("Vi4", Vi4, response.TOTAL);
          
          $("#Vi5").html("<strong>" + Vi5 + "</strong>");
          changeColors("Vi5", Vi5, response.TOTAL);
          
          $("#Vi6").html("<strong>" + Vi6 + "</strong>");
          changeColors("Vi6", Vi6, response.TOTAL);
          
          $("#Vi7").html("<strong>" + Vi7 + "</strong>");
          changeColors("Vi7", Vi7, response.TOTAL);
          
          $("#Vi8").html("<strong>" + Vi8 + "</strong>");
          changeColors("Vi8", Vi8, response.TOTAL);
          
          $("#Vi9").html("<strong>" + Vi9 + "</strong>");
          changeColors("Vi9", Vi9, response.TOTAL);

        },
        error: function(errorMsg) 
        {
          alert(errorMsg.statusTxt);
        } 
      });
    });

  });