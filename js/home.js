function loadProjects()
{
  var dataToSend = { "action": "GETPROJECTS"};
  $.ajax({
    type: "POST",
    url: "data/applicationLayer.php",
    data: dataToSend,
    dataType: "json",
    contentType: "application/x-www-form-urlencoded",
    success: function(jsonData) {
      var currentHTML = '<div class="column is-third" id="addProject"> <div class="card is-fullwidth"> <header class="card-header"> <p class="card-header-title"> Add A Project </p> </header>' +
          '<div class="card-content"> <div class="content"> <p><img src="img/add.png" style="width:215px;height:215px;"></p> </div> </div> </div> </div>';

      var projectName;
      var description;
      var finalDate;
      var createdBy;
      var currentLength = jsonData.length;


      for (i = 0; i < currentLength; i++) {
        projectName = jsonData[i].name;
        description = jsonData[i].description;
        finalDate = jsonData[i].fechaFinal;
        createdBy = jsonData[i].createdBy;
        var urlToEdit = 'project.php?' + 'name=' + projectName + "&CreatedBy=" + createdBy;
        currentHTML += '<div class="column is-third"> <div class="card is-fullwidth"> <header class="card-header">' +
          '<p class="card-header-title">' + projectName + '</p> </header> <div class="card-content"> <div class="content">' +
          '<p>' + description +  '</p> <p class="createdBy"><strong>Created by: </strong>' + createdBy + '</p><p><strong> Due date: </strong> ' + finalDate + '</p> </div> </div> <footer class="card-footer">' + 
          '<a class="card-footer-item" href="' + urlToEdit + '">Edit</a> <a class="card-footer-item deleteButton">Delete</a> </footer> </div> </div>';
      }

      $("#projects").html(currentHTML);
    },
    error: function(errormsg) {
      alert(errormsg.statusText);
      window.location.replace("home.php"); 
    }
  });
}

$(document).ready(function () {

  $("#logout").on("click", function() {
    var dataToSend = {"action": "LOGOUT"};

    $.ajax({
      type: "POST",
      url: "data/applicationLayer.php",
      data: dataToSend,
      dataType: "json",
      contentType: "application/x-www-form-urlencoded",
      success: function(sessionObjson) {
        alert("Thank you for using Papahana :) ")
        window.location.replace("index.html");
      },
      error: function(errormsg)
      {
        alert(errormsg.statusTxt);
        if(errormsg.statusTxt == 'Session has expired')
        {
          window.location.replace("index.html");
        }
      }
    });
  });

  var dataToSend = { "action": "SESSION" };
  $.ajax({
    type: "POST",
    url: "data/applicationLayer.php",
    data: dataToSend,
    dataType: "json",
    contentType: "application/x-www-form-urlencoded",
    success: function(sessionObjson) {

    },
    error: function(errormsg)
    {
      alert(errormsg.statusTxt);
      if(errormsg.statusTxt == 'Session has expired')
      {
        window.location.replace("index.html");
      }
    }
  });

  loadProjects();

  $("#projects").on("click", "#addProject", function(){
    window.location.replace("addProject.php");
  });
  $("#projects").on("click", ".deleteButton", function() {
    var projectName = $(this).parent().parent().find("header").text().trim();
    var createdBy = $(this).parent().parent().find("div.content").children('.createdBy').text().substring(12).trim();
    var dataToSend = {"action": "DELETEPROJECT",
                      "projectName": projectName,
                      "createdBy": createdBy };

    $.ajax({
      type: "POST",
      url: "data/applicationLayer.php",
      data: dataToSend,
      dataType: "json",
      contentType: "application/x-www-form-urlencoded",
      success: function(sessionObjson) {
        loadProjects();
      },
      error: function(errormsg)
      {
        alert(errormsg.statusTxt);
        if(errormsg.statusTxt == 'Session has expired')
        {
          window.location.replace("index.html");
        }
      }
    });

  });
});