schedule = {action:"SCHEDULE", Lu1:1, Lu2:1, Lu3:1, Lu4:1, Lu5:1, Lu6:1, Lu7:1, Lu8:1, Lu9:1, Ma1:1, Ma2:1, Ma3:1, Ma4:1, Ma5:1, Ma6:1, Ma7:1, Ma8:1, Ma9:1, Mi1:1, Mi2:1, Mi3:1, Mi4:1, Mi5:1, Mi6:1, Mi7:1, Mi8:1, Mi9:1, Ju1:1, Ju2:1, Ju3:1, Ju4:1, Ju5:1, Ju6:1, Ju7:1, Ju8:1, Ju9:1, Vi1:1, Vi2:1, Vi3:1, Vi4:1, Vi5:1, Vi6:1, Vi7:1, Vi8:1, Vi9:1 }

function changeSchedule(id){
    if (schedule[id] == 1){
        $("#"+id).removeClass("is-success");
        $("#"+id).addClass("is-danger");
        $("#"+id).val("Busy");
        schedule[id] = 0;
    }
    else{
        $("#"+id).removeClass("is-danger");
        $("#"+id).addClass("is-success");
        $("#"+id).val("Free");
        schedule[id] = 1;
    }
}

$(document).ready(function() {

    $("#logout").on("click", function() {
        var dataToSend = {"action": "LOGOUT"};

        $.ajax({
            type: "POST",
            url: "data/applicationLayer.php",
            data: dataToSend,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            success: function(sessionObjson) {
                alert("Thank you for using Papahana :) ")
                window.location.replace("index.html");
            },
            error: function(errormsg)
            {
                alert(errormsg.statusTxt);
                if(errormsg.statusTxt == 'Session has expired')
                {
                    window.location.replace("index.html");
                }
            }
        });
    });

    $(".schedule").unbind().click (function(){
        var id = $(this).attr('id');
        changeSchedule(id);
    });
    $("#submitSchedule").on("click", function(){
        $.ajax({
            url: "data/applicationLayer.php",
            type: "POST",
            data: schedule,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            success: function(jsonObject) {
                window.location.replace("home.php");
            },
            error: function(errorMsg) {
                alert("El usuario o la contraseña no son válidos");
            }
        }); 
    });            
});