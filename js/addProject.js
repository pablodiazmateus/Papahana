$(document).ready(function() {

    $("#logout").on("click", function() {
        var dataToSend = {"action": "LOGOUT"};

        $.ajax({
            type: "POST",
            url: "data/applicationLayer.php",
            data: dataToSend,
            dataType: "json",
            contentType: "application/x-www-form-urlencoded",
            success: function(sessionObjson) {
                alert("Thank you for using Papahana :) ")
                window.location.replace("index.html");
            },
            error: function(errormsg)
            {
                alert(errormsg.statusTxt);
                if(errormsg.statusTxt == 'Session has expired')
                {
                    window.location.replace("index.html");
                }
            }
        });
    });

    $("#createProject").on("click", function() {
        var valReg = true;     
        if ($("#projectName").val() == ""){
            valReg = false;
        }
        if ($("#finalDate").val() == ""){
            valReg = false;
        }
        if ($("#description").val() == ""){
            valReg = false;
        }

        if(valReg){
            var dataToSend = { "action": "NEWPROJECT",
                              "projectName": $("#projectName").val(),
                              "fechaFinal": $("#finalDate").val(),
                              "description": $("#description").val()
                             };
            $.ajax({
                type: "POST",
                url: "data/applicationLayer.php",
                data: dataToSend,
                dataType: "json",
                contentType: "application/x-www-form-urlencoded",
                success: function(sessionObjson) 
                {
                    console.log(sessionObjson);
                    window.location.replace("home.php");
                },
                error: function(errormsg){
                    alert(errormsg.statusText); 
                }
            });
        }
    });            
});
