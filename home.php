<!DOCTYPE html>
<html>
  <head>
    <title>Papahana</title>
    <meta charset = "UTF-8" />
    <script src="js/jquery-2.2.0.js" type="text/javascript"> </script>
    <script src="js/home.js" type="text/javascript"> </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.18/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>

    <header class="header">
      <div class="container">
        <!-- Left side -->
        <div class="header-left">
          <span class="header-item">
            <a href="home.php"><i class="fa fa-bar-chart logo" aria-hidden="true">Papahana</i></a>
          </span>
        </div>
        <div class="header-right">
          <span class="header-item"> <a class="button is-danger" id="logout"> Logout <span class="icon"> <i class="fa fa-sign-out" aria-hidden="true"></i></span></a> </span>
        </div>
      </div>
    </header>

    <div class="container is-fluid wrapper">
      <div class="columns is-multiline" id="projects">

      </div>
    </div>

    <br>

    <footer class="footer">
      <div class="container">
        <div class="content is-centered">
          <p class="logo">
            <strong>Papahana</strong> by <a href="about.html">Pablo Díaz</a> and <a href="about.html">Alex de la Rosa.</a> For updates click  <a href="news.html">here</a>
          </p>
        </div>
      </div>
    </footer>
  </body>
</html>