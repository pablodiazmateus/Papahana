<!DOCTYPE html>
<html>
  <head>
    <title>Papahana</title>
    <meta charset = "UTF-8" />
    <script src="js/jquery-2.2.0.js" type="text/javascript"> </script>
    <script src="js/project.js" type="text/javascript"> </script>
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.5.0/css/font-awesome.min.css"/>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/bulma/0.0.18/css/bulma.min.css"/>
    <link rel="stylesheet" type="text/css" href="css/style.css"/>
  </head>
  <body>

    <header class="header">
      <div class="container">
        <!-- Left side -->
        <div class="header-left">
          <span class="header-item">
            <a href="home.php"><i class="fa fa-bar-chart logo" aria-hidden="true">Papahana</i></a>
          </span>
        </div>
        <div class="header-right">
          <span class="header-item"> <a class="button is-danger" id="logout"> Logout <span class="icon"> <i class="fa fa-sign-out" aria-hidden="true"></i></span></a> </span>
        </div>
      </div>
    </header>


    <div class="tabs is-toggle is-fullwidth" id="tabselector">
      <div id="menu">
        <ul id="tabs">
          <li class="is-active selected" id="tab1"><a><i class="fa fa-check-square-o" aria-hidden="true"></i>Project</a></li>
          <li id="tab2"><a><i class="fa fa-pencil-square-o" aria-hidden="true"></i>Edit Project </a></li>
          <li id="tab3"><a><i class="fa fa-calendar-check-o" aria-hidden="true"></i>When To Meet</a></li>
        </ul>
      </div>

      <div id="tabcontent">

        <div id="content-tab1" class="active ">
          <div class="container is-fluid">
            <span id="projectName"><strong>Name:</strong> <?php echo $_GET['name']; ?></span>
            <br/>
            <span id="description">Description: </span>
            <br/>
            <span id="collaborators">Collaborators: </span>
            <br/>
            <span id="createdBy"><strong>Created By:</strong> <?php echo $_GET['CreatedBy']; ?></span>
            <br/>
            <span id="dueDate">Due Date:</span>
            <br/>       
            <!--            <span id="tasksList"> Tasks: </span>-->
            <br/>

            <table id="taskTable">

            </table>

            <br>
            <div id="progressBar">
              <progress class="progress is-success" value="50" max="100"></progress>
            </div>
          </div>
        </div>


        <div id="content-tab2" class="">
          <div class="container is-fluid">
            <form>
              <h1 class="title is-1">Edit</h1>
              <p class="control">
                <input class="input" id="finalDate" type="text" placeholder="New due date"/>
              </p>

              <p class="control">
                <textarea class="textarea" id="newDescription" placeholder="New description" ></textarea>
              </p>

              <br>
              <p>
                <input class="button is-info" id="updateProject" type="submit" value="Save project">
              </p>
              <br>
            </form>

            <h1 class="title is-1">Add Collaborators</h1>
            <div id="Search">
              <p class="control has-addons">
                <input class="input" type="text" placeholder="Search a Collaborator" id="search">
                <a class="button is-info" id="searchButton">
                  Search
                </a>
              </p>
            </div>
            <div id="Results">
              <table id="searchTable">

              </table>
            </div>
          </div>
        </div>

        <div id="content-tab3" class="">
          <div class="container is-fluid">
            <h1 class="title is-1">When to meet</h1>
            <table class="is-bordered is-striped is-narrow">
              <tr>
                <th>Hour</th><th>Monday</th><th>Tuesday</th><th>Wednesday</th><th>Thursday</th><th>Friday</th>
              </tr>
              <tr>
                <td>07:30</td><td id="Lu1"></td><td id="Ma1"></td><td id="Mi1"></td><td id="Ju1"></td><td id="Vi1"></td>
              </tr>
              <tr>
                <td>09:00</td><td id="Lu2"></td><td id="Ma2"></td><td id="Mi2"></td><td id="Ju2"></td><td id="Vi2"></td>
              </tr>
              <tr>
                <td>10:30</td><td id="Lu3"></td><td id="Ma3"></td><td id="Mi3"></td><td id="Ju3"></td><td id="Vi3"></td>
              </tr>
              <tr>
                <td>12:00</td><td id="Lu4"></td><td id="Ma4"></td><td id="Mi4"></td><td id="Ju4"></td><td id="Vi4"></td>
              </tr>
              <tr>
                <td>13:30</td><td id="Lu5"></td><td id="Ma5"></td><td id="Mi5"></td><td id="Ju5"></td><td id="Vi5"></td>
              </tr>
              <tr>
                <td>15:00</td><td id="Lu6"></td><td id="Ma6"></td><td id="Mi6"></td><td id="Ju6"></td><td id="Vi6"></td>
              </tr>
              <tr>
                <td>16:30</td><td id="Lu7"></td><td id="Ma7"></td><td id="Mi7"></td><td id="Ju7"></td><td id="Vi7"></td>
              </tr>
              <tr>
                <td>18:00</td><td id="Lu8"></td><td id="Ma8"></td><td id="Mi8"></td><td id="Ju8"></td><td id="Vi8"></td>
              </tr>
              <tr>
                <td>19:30</td><td id="Lu9"></td><td id="Ma9"></td><td id="Mi9"></td><td id="Ju9"></td><td id="Vi9"></td>
              </tr>
            </table>
            <div> 

              <br>
              <br>
              <span class="colorTodos"> 90 to 100% of the team is available </span>
              <br>
              <span class="colorCasiTodos"> 75 to 89% of the team is available </span>
              <br>
              <span class="colorMedia"> 50 to 74% of the team is available </span>
              <br>
              <span class="colorBaja"> 30 to 49% of the team is available </span>
              <br>
              <span class="colorMuyBaja"> 0 to 30% of the team is available </span>
              <br>
              <br>
              <br>
              <br>
              <br>
            </div>
          </div>
        </div>

      </div> <!-- Cierro tab content-->
    </div> <!-- Cierro tab selector-->


    <footer class="footer">
      <div class="container">
        <div class="content is-centered">
          <p class="logo">
            <strong>Papahana</strong> by <a href="about.html">Pablo Díaz</a> and <a href="about.html">Alex de la Rosa.</a> For updates click  <a href="news.html">here</a>
          </p>
        </div>
      </div>
    </footer>
  </body>
</html>